import {DataSource, EntityRepository, Repository} from 'typeorm'
import {Post} from './post.model'

@EntityRepository(Post)
export class PostRepository extends Repository<Post> {

    constructor(dataSource: DataSource) {
        super(Post, dataSource.createEntityManager());
    }

    getInactivePosts() {
        return this.find({
            relations: {
                category: true
            }
        })
    }
}
