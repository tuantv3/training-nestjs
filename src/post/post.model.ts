import {
    BaseEntity, Column, CreateDateColumn, Entity,
    ManyToOne, OneToMany, PrimaryGeneratedColumn, Unique, UpdateDateColumn
} from "typeorm";
import {Category} from "../category/category.model";
import internal from "stream";

@Entity({name: 'post'})
export class Post extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    title: string

    @Column()
    desc: string

    @Column()
    note: string

    @Column()
    categoryId: bigint

    // define relationship with profiles table
    @ManyToOne(() => Category, (cate) => cate.id)
    category: Category;

    constructor(partial: Partial<Post>) {
        super()
        Object.assign(this, partial)
    }
}