import {Body, Controller, Get, Post, Req, Res} from "@nestjs/common";
import {PostService} from "./post.service";
import {response} from "../support/ResponseResource";
import {Request, Response} from "express";

@Controller("post")
export class PostController {
    constructor(private readonly postService: PostService) {
    }

    @Get()
    async getHello(@Req() req: Request, @Res() res: Response) {
        const data = await this.postService.getList();
        return response(res, data);
    }

    @Post("create")
    creaetdPost(@Body() post: any) {
        return this.postService.getList();
    }
}

