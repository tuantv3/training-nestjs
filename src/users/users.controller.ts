import {Body, Controller, Get, Post, Req, Res} from "@nestjs/common";
import {response} from "../support/ResponseResource";
import {Request, Response} from "express";
import {UsersService} from "./users.service";
import {FormDataRequest} from "nestjs-form-data";
import {CreateUser} from "./users.model";

@Controller("users")
export class UsersController {
    constructor(private readonly usersService: UsersService) {
    }

    @Post('register')
    @FormDataRequest()
    async register(@Body() body: CreateUser, @Res() res: Response) {
        const data = await this.usersService.create(body);
        return response(res, data)
    }
}

