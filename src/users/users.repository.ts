import {DataSource, EntityRepository, Repository} from 'typeorm'
import {Users} from "./users.model";

@EntityRepository(Users)
export class UsersRepository extends Repository<Users> {

    constructor(dataSource: DataSource) {
        super(Users, dataSource.createEntityManager());
    }
}
