import {Module} from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {UsersRepository} from "./users.repository";
import {UsersService} from "./users.service";
import {UsersController} from "./users.controller";
import {NestjsFormDataModule} from "nestjs-form-data";
import {ConfigService} from "@nestjs/config";
import {StorageAmazonBucketsS3} from "../support/StorageAmazonBucketsS3";

@Module({
    imports: [NestjsFormDataModule, TypeOrmModule.forFeature([
        UsersRepository
    ]),
    ],
    controllers: [UsersController],
    providers: [UsersService, UsersRepository, ConfigService, StorageAmazonBucketsS3],
    exports: [
        TypeOrmModule,
    ],
})
export class UsersModule {
}
