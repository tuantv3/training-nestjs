import {
    BaseEntity, Column, CreateDateColumn, Entity,
    ManyToOne, OneToMany, PrimaryGeneratedColumn, Unique, UpdateDateColumn
} from "typeorm";
import {Category} from "../category/category.model";
import internal from "stream";
import {Exclude} from "class-transformer";
import {IsEmail, IsNotEmpty} from "class-validator";

@Entity({name: 'users'})
export class Users extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    name: string

    @Column()
    user_name: string

    @Column()
    email: string

    @Column()
    image: string

    @Column()
    @Exclude({toPlainOnly: true})
    password: string;

    constructor(partial: Partial<Users>) {
        super()
        Object.assign(this, partial)
    }
}

export class CreateUser {
    @IsEmail()
    @IsNotEmpty()
    email: string;

    @IsNotEmpty()
    password: string;
}