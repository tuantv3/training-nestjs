import {Controller, Get, Post, Request, Res, UseGuards} from '@nestjs/common';
import {AuthGuard} from '@nestjs/passport';
import {AuthService} from './auth.service';
import {JwtAuthGuard} from './jwt-auth.guard';
import {UsersService} from "../users/users.service";
import {response, unauthorized} from "../support/ResponseResource";

@Controller('auth')
export class AuthController {

    constructor(private authService: AuthService, private usersService: UsersService) {
    }

    @Post('login')
    async login(@Request() req, @Res() res) {
        const data = await this.authService.validateUserCredentials(req.body.email, req.body.password)

        if (data) {
            const token = await this.authService.loginWithCredentials(req.body);

            return response(res, {
                "profile": data,
                "token": "Bearer" + ' ' + token.access_token
            })
        }

        return unauthorized(res)
    }

    @UseGuards(JwtAuthGuard)
    @Get('user-info')
    async getUserInfo(@Request() req, @Res() res) {
        const data = await this.usersService.getUsers();
        return response(res, data);
    }
}


