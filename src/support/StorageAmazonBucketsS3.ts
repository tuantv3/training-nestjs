import {S3} from 'aws-sdk';
import {v4 as uuid} from 'uuid';
import {UsersRepository} from "../users/users.repository";

export class StorageAmazonBucketsS3 {

    async uploadFileTos3(image, path: string) {
        const s3 = new S3();
        return await s3.upload({
            Bucket: path,
            Body: image.buffer,
            Key: `${image.originalName}`,
        }).promise();
    }
}