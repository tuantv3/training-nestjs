import {Response} from "express";

export function response(res: Response<any, Record<string, any>>, data: unknown) {
    if (data) {
        res.status(200).json({
            code: 200,
            message: "Success",
            data: data
        });
    } else {
        res.status(500).json({
            code: 500,
            message: "An error occurred"
        });
    }
}

export function forbidden(res: { status: (arg0: number) => { (): any; new(): any; json: { (arg0: { code: number; message: string; error: any; }): void; new(): any; }; }; }, error: any) {
    res.status(403).json({
        code: 403,
        message: "Forbidden",
        error: error
    });
}

export function unauthorized(res: { status: (arg0: number) => { (): any; new(): any; json: { (arg0: { code: number; message: string; error: any; }): void; new(): any; }; }; }, error?: any) {
    res.status(401).json({
        code: 401,
        message: "Unauthorized",
        error: error
    });
}

export function validation(res: { status: (arg0: number) => { (): any; new(): any; json: { (arg0: any): void; new(): any; }; }; }, error: any) {
    res.status(422).json({
        code: 422,
        message: "Validate error",
        ...error
    });
}

