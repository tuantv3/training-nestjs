import {Injectable} from "@nestjs/common";
import {CategoryRepository} from "./category.repository";

@Injectable()
export class CategoryService {
    constructor(private readonly categoryRepository: CategoryRepository) {
    }

    async getList() {
        return new Promise((resolve, reject) => {
            try {
                const data = this.categoryRepository.getInactivePosts();
                resolve(data);
            } catch (e) {
                reject(e);
            }
        });
    }

    create(body) {
        return new Promise((resolve, reject) => {
            try {
                const data = this.categoryRepository.create(body);
                resolve(data)
            } catch (e) {
                console.log(e);
                reject(e)
            }
        })
    }

}
