import {IsEmail, IsInt, isInt, IsNotEmpty} from "class-validator";
import {
    BaseEntity,
    Column,
    Entity, OneToMany,
    PrimaryGeneratedColumn,
} from "typeorm";
import {Post} from "../post/post.model";

@Entity({name: 'category'})
export class Category extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number

    @Column()
    category_name: string

    @Column()
    image: string

    // define relationship with profiles table
    @OneToMany(() => Post, (cate) => cate.category)
    post: Post;

    constructor(partial: Partial<Category>) {
        super()
        Object.assign(this, partial)
    }
}

export class CreateCategory {
    image: string;
    category_name: string;
}